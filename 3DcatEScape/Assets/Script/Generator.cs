using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public GameObject iiyatuPrefab;
    public GameObject waruiyatuPrefab;
    public GameObject Totemoiiyatu;
    float span = 1.0f;
    float specialspan = 5.0f;
    float delta = 0;
    float specialdelta = 0.2f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(iiyatuPrefab) as GameObject;
            int px = Random.Range(-7, 7);
            go.transform.position = new Vector3(15, 1, px);

        }
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go1 = Instantiate(waruiyatuPrefab) as GameObject;
            int px1 = Random.Range(-7, 7);
            go1.transform.position = new Vector3(15, 1, px1);

        }
        this.specialdelta += Time.deltaTime;
        if (this.specialdelta > this.specialspan)
        {
            this.specialdelta = 0;
            GameObject go2 = Instantiate(Totemoiiyatu) as GameObject;
            int px2 = Random.Range(-7, 7);
            go2.transform.position = new Vector3(15, 0.75f, px2);

        }
    }
}
