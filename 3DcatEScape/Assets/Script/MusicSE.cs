using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSE : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource hornsound;

    void Start()
    {
        hornsound = GetComponent<AudioSource>();
    }
    //ボタンをクリックした時のスクリプト。
    public void OnClick()
    {
        hornsound.PlayOneShot(hornsound.clip);
    }
}
